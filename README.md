### Paypal subscription example

This is a simple application demonstrating subscribing for a recurring payment, cancelling a subscription
and request for refund.

##### Setup
1. Setup the code on your PC. To do this, you **clone** the code from version control.

    git clone <repo-address>

2. Navigate to the project directory and install requirements.

   cd paypal_subscription_example/
   pip install -r requirements.txt

3. Migrate and syncdb

   ./manage.py migrate
   ./manage.py syncdb

   *Ensure you create a superuser when you syncdb. This user is the one you will use for the demo*

4. Run the development server

    ./manage.py runserver

You can then access the application on [http://127.0.0.1:8000](http://127.0.0.1:8000)

**NOTE**
The various API settings for the seller account are defined in the settings module. These should be modified accordingly.
They are:
- CLIENT_ID
- CLIENT_SECRET
- PAYPAL_RECEIVER_EMAIL
- PAYPAL_WPP_USER
- PAYPAL_WPP_PASSWORD
- PAYPAL_WPP_SIGNATURE

There are also various URL settings for endpoints and the sandbox settings for Paypal. These should also be adjusted
accordingly. They are:
- PAYPAL_TEST
- TRANSACTION_ENDPOINT
- EXPRESS_CHECKOUT_URL
- RETURN_URL
- CANCEL_URL


