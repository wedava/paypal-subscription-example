# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subscribe', '0002_auto_20150710_0517'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='payerid',
            name='user',
        ),
        migrations.DeleteModel(
            name='PayerID',
        ),
    ]
