from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from .views import *

urlpatterns = [

    url(r'^$', index, name='index'),
    url(r'^action/$', subscribe, name='subscribe'),
    url(r'^cancel/(?P<profileid>[\w\-]+)/$', cancel, name='cancel'),
    url(r'^refund/(?P<transactionid>[\w\-]+)$', refund, name='refund'),
    # url(r'^ipn/', include('paypal.standard.ipn.urls')),
    url(r'^admin/', include(admin.site.urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)