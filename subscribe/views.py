from django.template.context import RequestContext
from django.shortcuts import render_to_response, HttpResponseRedirect
# from paypal.standard.forms import PayPalPaymentsForm
# from paypal.standard.models import ST_PP_COMPLETED
# from paypal.standard.ipn.signals import valid_ipn_received
from django.core.urlresolvers import reverse
from django.contrib import messages
from paypal.pro.helpers import PayPalWPP
from paypal.pro.models import PayPalNVP
from paypal.pro.exceptions import PayPalFailure
import datetime
from django.contrib.auth.decorators import login_required
from dateutil.relativedelta import *
import requests
from django.conf import settings
from urllib.parse import unquote, parse_qsl, parse_qs
import collections
import socket


# def subscribe(request):
#     """View to handle rendering of index page and subscription"""
#     paypal_dict = {
#         "cmd": "_xclick-subscriptions",
#         "a3": "9.99",   #monthly price
#         "p3": 1,   #duration of each unit (depends on unit)
#         "t3": "M",   #duration unit ("M for Month")
#         "src": "1",   #make payments recur
#         "sra": "1",   #reattempt payment on payment error
#         "no_note": "1",   #remove extra notes (optional)
#         "item_name": "my cool subscription",
#         "notify_url": "https://183e479b.ngrok.com/ipn/",
#         "return_url": "https://183e479b.ngrok.com",
#         "cancel_return": "https://183e479b.ngrok.com/cancel/",
#     }
#     form = PayPalPaymentsForm(initial=paypal_dict, button_type="subscribe")
#     return render_to_response("subscribe.html", locals(), context_instance=RequestContext(request))
#
# def subscription_successful(request, sender, **kwargs):
#     ipn_obj = sender
#     if ipn_obj.payment_status == ST_PP_COMPLETED:
#         messages.add_message(request, messages.SUCCESS, 'Subscription successful')
#         return HttpResponseRedirect(reverse('subscribe'))
#     else:
#         messages.add_message(request, messages.ERROR, 'Subscription failed')
#         return HttpResponseRedirect(reverse('subscribe'))
#
# valid_ipn_received.connect(subscription_successful)

def slicedict(d, s):
    return {k: v for k, v in d.items() if k.startswith(s)}


@login_required
def index(request):
    try:
        transaction_endpoint = settings.TRANSACTION_ENDPOINT
        payload = {
            'user': settings.PAYPAL_WPP_USER,
            'pwd': settings.PAYPAL_WPP_PASSWORD,
            'signature': settings.PAYPAL_WPP_SIGNATURE,
            'method': 'TransactionSearch',
            'startdate': datetime.datetime.today() - relativedelta(months=+1),
            'profileid': PayPalNVP.objects.get(user=request.user, method="CreateRecurringPaymentsProfile").profileid,
            'version': '94'
        }
        t = requests.post(transaction_endpoint, data=payload)
        t1 = unquote(t.text)
        t2 = parse_qsl(t1)
        t3 = collections.OrderedDict(t2)
        import sys
        sys.stderr.write(str(t1))
        transactions1 = collections.OrderedDict(slicedict(t3, 'L_TRANSACTIONID'))
        list1 = list(transactions1.values())
        transactions2 = collections.OrderedDict(slicedict(t3, 'L_STATUS'))
        list2 = list(transactions2.values())
        transactions3 = collections.OrderedDict(slicedict(t3, 'L_TIMESTAMP'))
        list3 = list(transactions3.values())
        indices = [i for i, x in enumerate(list2) if x == "Completed"]
        import sys
        sys.stderr.write(str(indices))
        refundable = []
        dates = []
        for i in indices:
            key = "{0}".format(i)
            refundable.append(list1[i])
            dates.append(list3[i])
        refundables = {}
        out = zip(dates, refundable)
        count = 0
        for i in out:
            key = str(count)
            refundables[key] = {'date': i[0], 'transactionid': i[1]}
            count += 1

    except socket.gaierror:
        messages.add_message(request, messages.ERROR, "Network error")
        transactions = ""
    except Exception as e:
        import sys
        sys.stderr.write(str(e))
        transactions = ""
    if request.POST:
        params = {
            'method': "SetExpressCheckout",
            'noshipping': 1,
            'desc': "Test recurring payments profile",
            'paymentaction': 'Authorization',
            'returnurl': settings.RETURN_URL,
            'cancelurl': settings.CANCEL_URL,
            'amt': '9.99',
            'currencycode': 'USD',
            'l_billingtype0': 'RecurringPayments',
            'l_billingagreementdescription0': "Test recurring payments profile",
            'paymentrequest_0_itemamt': '9.99',
            'l_paymentrequest_0_amt0': '9.99'
        }
        # token = PayPalWPP.setExpressCheckout(params=params)
        obj = PayPalWPP(request)
        nvp = obj.setExpressCheckout(params=params)
        url = settings.EXPRESS_CHECKOUT_URL + nvp.token
        return HttpResponseRedirect(url)
    try:
        subscription = PayPalNVP.objects.get(user=request.user, method="CreateRecurringPaymentsProfile")
    except PayPalNVP.DoesNotExist:
        pass
    return render_to_response("subscribe.html", locals(), context_instance=RequestContext(request))


@login_required
def subscribe(request):
    token = request.GET['token']
    obj = PayPalWPP(request)
    params1 = {
        'profilestartdate': datetime.datetime.today(),
        'desc': "Test recurring payments profile",
        'billingperiod': 'Month',
        'billingfrequency': '1',
        'amt': '9.99',
        'token': token,
        'email': 'arlusishmael-buyer@gmail.com',
        'currencycode': 'USD',
        'initamt': '9.99',  # Charge the amount immediately
    }
    try:
        create_recurring = obj.createRecurringPaymentsProfile(params=params1)
    except Exception:
        messages.add_message(request, messages.ERROR, "Error trying to create subscription")
        return HttpResponseRedirect(reverse('index'))
    messages.add_message(request, messages.SUCCESS, "Your subscription is now active")
    return HttpResponseRedirect(reverse('index'))


@login_required
def cancel(request, profileid):
    obj = PayPalWPP(request)
    params = {
        'profileid': profileid,
        'action': 'Cancel'
    }
    try:
        obj.manangeRecurringPaymentsProfileStatus(params=params)
        saved_profile = PayPalNVP.objects.get(profileid=profileid, method="CreateRecurringPaymentsProfile")
        saved_profile.delete()
    except PayPalFailure:
        messages.add_message(request, messages.ERROR, "Error cancelling subscription")
        return HttpResponseRedirect(reverse('index'))
    messages.add_message(request, messages.SUCCESS, "Subscription canceled")

    return HttpResponseRedirect(reverse('index'))


@login_required
def refund(request, transactionid):
    transaction_endpoint = "https://api-3t.sandbox.paypal.com/nvp"
    payload = {
        'user': settings.PAYPAL_WPP_USER,
        'pwd': settings.PAYPAL_WPP_PASSWORD,
        'signature': settings.PAYPAL_WPP_SIGNATURE,
        'method': 'RefundTransaction',
        'transactionid': transactionid,
        'refundtype': 'Full',
        'refundsource': 'any',
        'version': '94'
    }
    transactions = requests.post(transaction_endpoint, data=payload).text
    t1 = unquote(transactions)
    t2 = parse_qs(t1)
    import sys
    sys.stderr.write(str(t2))
    if t2['ACK'][0] == "Failure":
        error_msg = t2["L_LONGMESSAGE0"][0] + "Subscription is not refundable"
        messages.add_message(request, messages.ERROR, error_msg)
    else:
        messages.add_message(request, messages.SUCCESS, "Request for refund successful")
    return HttpResponseRedirect(reverse('index'))
